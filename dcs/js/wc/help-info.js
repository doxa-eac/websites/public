class HelpInfo extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    this.shadowDOM = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.shadowDOM.innerHTML = `
    <style> 
      @import url('../../css/bootstrap.min.css');
      @import url('../../img/bootstrap-icons.css');
    </style>
    <style>
        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
    </style>
    <div class="alert alert-primary" role="alert">
        <i class="bi-info-circle" style="color: blue;"></i>
        ${this.text}
    </div>
    `
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['text'];
  }

  attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get text() {
    return this.getAttribute("text");
  }

  set text(value) {
    this.setAttribute("text", value);
  }

}

window.customElements.define('help-info', HelpInfo);